package test.app.services;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

import test.app.domain.UserInfo;

public class UserServiceTest {

    @Mock HazelcastInstance hazelcastInstance;
    @Mock IMap<Integer, UserInfo> imap;
    UserService userService;
    
    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        when(hazelcastInstance.getMap(UserService.USERS_MAP)).thenReturn((IMap) imap);
        userService = new UserService(hazelcastInstance);
    }
    
    @Test
    public void testAddNewUserInfo() {
        Integer userId = 1;
        Integer levelId = 32;
        Integer result = 1245;
        
        userService.addUserTopLevelResult(userId, levelId, result);

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<UserInfo> infoCaptor = ArgumentCaptor.forClass(UserInfo.class);
        verify(imap).put(idCaptor.capture(), infoCaptor.capture());
        assertThat(idCaptor.getValue(), is(userId));
        assertThat(infoCaptor.getValue().getId(), is(userId));
        assertThat(infoCaptor.getValue().getLevelTopResult(levelId), is(result));
    }
    
    @Test
    public void testUpdateUserInfoResult() {
        Integer userId = 1;
        Integer levelId = 32;
        Integer oldResult = 2000;
        Integer newResult = 3000;
        
        UserInfo info = prepareUserInfo(userId, levelId, oldResult);
        when(imap.get(userId)).thenReturn(info);
        
        userService.addUserTopLevelResult(userId, levelId, newResult);
        
        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<UserInfo> infoCaptor = ArgumentCaptor.forClass(UserInfo.class);
        verify(imap).put(idCaptor.capture(), infoCaptor.capture());
        assertThat(idCaptor.getValue(), is(userId));
        assertThat(infoCaptor.getValue().getId(), is(userId));
        assertThat(infoCaptor.getValue().getLevelTopResult(levelId), is(newResult));
    }
    
    @Test
    public void testAddNewUserInfoResult() {
        Integer userId = 1;
        Integer levelId = 32;
        Integer result = 2000;
        Integer anotherLevelId = 45;
        Integer anotherLevelResult = 3000;
        
        UserInfo info = prepareUserInfo(userId, levelId, result);
        when(imap.get(userId)).thenReturn(info);
        
        userService.addUserTopLevelResult(userId, anotherLevelId, anotherLevelResult);
        
        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<UserInfo> infoCaptor = ArgumentCaptor.forClass(UserInfo.class);
        verify(imap).put(idCaptor.capture(), infoCaptor.capture());
        assertThat(idCaptor.getValue(), is(userId));
        assertThat(infoCaptor.getValue().getId(), is(userId));
        assertThat(infoCaptor.getValue().getLevelTopResult(levelId), is(result));
        assertThat(infoCaptor.getValue().getLevelTopResult(anotherLevelId), is(anotherLevelResult));
    }
    
    @Test
    public void testTrySetLowerLevelResult() {
        Integer userId = 1;
        Integer levelId = 32;
        Integer oldResult = 3000;
        Integer newResult = 2000;
        
        UserInfo info = prepareUserInfo(userId, levelId, oldResult);
        when(imap.get(userId)).thenReturn(info);
        
        userService.addUserTopLevelResult(userId, levelId, newResult);
        
        verify(imap, never()).put(null, null);
    }

    private UserInfo prepareUserInfo(Integer userId, Integer levelId, Integer result) {
        Map<Integer, Integer> topLevelsResult = new HashMap<>();
        topLevelsResult.put(levelId, result);
        return new UserInfo(userId, topLevelsResult);
    }

}
