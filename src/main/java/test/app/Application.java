package test.app;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.sun.net.httpserver.HttpServer;

import test.app.services.UserService;
import test.app.web.handlers.LevelInfoHandler;
import test.app.web.handlers.ServerStopHandler;
import test.app.web.handlers.SetInfoHandler;
import test.app.web.handlers.TestHandler;
import test.app.web.handlers.UserInfoHandler;
import test.app.web.mappers.JsonObjectMapper;
import test.app.web.mappers.ObjectMapper;

public class Application {

    private final static Logger log = LoggerFactory.getLogger(Application.class);
    
    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        
        Integer serverPort = Integer.getInteger("test.server.port", 8080);

        HttpServer server = HttpServer.create();
        server.bind(new InetSocketAddress(serverPort), 0);
        
        HazelcastInstance hazelcastInstance = getHazelcastInstance();
        UserService userService = new UserService(hazelcastInstance);
        ObjectMapper objectMapper = new JsonObjectMapper();
        
        new SetInfoHandler(objectMapper, userService).registerContext(server);
        new UserInfoHandler(objectMapper, userService).registerContext(server);
        new LevelInfoHandler(objectMapper, userService).registerContext(server);

        new ServerStopHandler(server).registerContext(server);
        new TestHandler(userService).registerContext(server);
        
        server.start();
        log.info("server started at " + serverPort + " in " + (System.currentTimeMillis() - start) + " ms");
    }
    
    private static HazelcastInstance getHazelcastInstance() {
        String defaultType = "server";
        String hazelcastTtype = System.getProperty("test.hazelcast.type", defaultType);
        if(defaultType.equals(hazelcastTtype)) {
            log.info("init hazelcast server");
            Config config = new Config("common");
            return Hazelcast.newHazelcastInstance(config);
        } else {
            log.info("init hazelcast client");
            String hazelcastAddress = System.getProperty("test.hazelcast.address", defaultType);
            ClientConfig clientConfig = new ClientConfig();
            ClientNetworkConfig networkConfig = clientConfig.getNetworkConfig();
            networkConfig.addAddress(hazelcastAddress);
            return HazelcastClient.newHazelcastClient(clientConfig);
        }
    }

}
