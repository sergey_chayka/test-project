package test.app.services;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

import test.app.domain.UserInfo;

public class UserService {

    protected final static String USERS_MAP = "users";
    
    private final HazelcastInstance hazelcastInstance;
    
    private final IMap<Integer, UserInfo> usersMap;
    
    public UserService(HazelcastInstance hazelcastInstance) {
        this.hazelcastInstance = hazelcastInstance;
        usersMap = this.hazelcastInstance.getMap(USERS_MAP);
    }
    
    public UserInfo getUserInfo(Integer userId) {
        return usersMap.get(userId);
    }
    
    public void saveUserInfo(UserInfo userInfo) {
        usersMap.put(userInfo.getId(), userInfo);
    }

    public void addUserTopLevelResult(Integer userId, Integer levelId, Integer newResult) {
        UserInfo userInfo = usersMap.get(userId);
        if(userInfo == null) {
            HashMap<Integer, Integer> map = new HashMap<>();
            map.put(levelId, newResult);
            userInfo = new UserInfo(userId, map);
            usersMap.put(userId, userInfo);
        } else {
            Integer oldResult = userInfo.getLevelTopResult(levelId);
            if(oldResult == null || oldResult < newResult) {
                HashMap<Integer, Integer> results = userInfo.getTopLevelResults();
                results.put(levelId, newResult);
                usersMap.put(userId, new UserInfo(userId, results));
            }
        }
    }
    
    /**
     * @param levelId
     * @param topCount
     * @return список пользователей только с результатом по указанному уровню
     */
    public List<UserInfo> getTopUsersOnLevel(Integer levelId, Integer topCount) {
        return usersMap
            .values(entry -> ((UserInfo) entry.getValue()).getLevelTopResult(levelId) != null)
            .stream()
            .sorted((u1, u2) -> u2.getLevelTopResult(levelId).compareTo(u1.getLevelTopResult(levelId)))
            .limit(topCount)
//            .map(user -> new UserInfo(user.getId(), Collections.singletonMap(levelId, user.getLevelTopResult(levelId))))
            .collect(Collectors.toList());
    }
}
