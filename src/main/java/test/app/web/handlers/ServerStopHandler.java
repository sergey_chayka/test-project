package test.app.web.handlers;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import test.app.web.HttpHelper;
import test.app.web.HttpStatus;

public class ServerStopHandler extends AbstractHandler {

    private final static Logger log = LoggerFactory.getLogger(ServerStopHandler.class);
    
    private final HttpServer server;

    public ServerStopHandler(HttpServer server) {
        this.server = server;
    }
    
    @Override
    protected void handleRequest(HttpExchange httpExchange) throws IOException {
        String body = HttpHelper.readStream(httpExchange.getRequestBody());
        JsonObject json = new JsonParser().parse(body).getAsJsonObject();
        int delaySeconds = json.get("delaySeconds").getAsInt();
        
        log.info("stopping server. shutdown in " + delaySeconds + " seconds");
        HttpHelper.sendResultJson(httpExchange, HttpStatus.OK, "{\"result\"=\"OK\"}");
        server.stop(delaySeconds);
    }

    @Override
    protected String getContextUrl() {
        return "/server/stop";
    }
}
