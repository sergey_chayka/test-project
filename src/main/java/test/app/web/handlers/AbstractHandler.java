package test.app.web.handlers;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import test.app.web.HttpHelper;
import test.app.web.HttpStatus;

public abstract class AbstractHandler implements HttpHandler {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    
    public void registerContext(HttpServer server) {
        server.createContext(getContextUrl(), this);
    }
    
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        try {
            handleRequest(httpExchange);
        } catch (Exception e) {
            handleError(httpExchange, e);
        }
    }
    
    protected void handleError(HttpExchange httpExchange, Exception exception) throws IOException {
        try {
            if(exception instanceof BadRequestException) {
                JsonObject object = new JsonObject();
                object.addProperty("result", exception.getMessage());
                HttpHelper.sendResultJson(httpExchange, HttpStatus.BAD_REQUEST, object.toString());
            } else {
                log.error("error", exception);
                JsonObject object = new JsonObject();
                object.addProperty("result", "ERROR");
                HttpHelper.sendResultJson(httpExchange, HttpStatus.INTERNAL_SERVER_ERROR, object.toString());
            }
        } catch (Exception e) {
            log.error("Can not send response message", e);
        }
    }
    
    abstract protected void handleRequest(HttpExchange httpExchange) throws IOException;

    abstract protected String getContextUrl();
}
