package test.app.web.handlers;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.sun.net.httpserver.HttpExchange;

import test.app.domain.UserInfo;
import test.app.services.UserService;
import test.app.web.HttpHelper;
import test.app.web.HttpStatus;
import test.app.web.dto.UserInfoDto;
import test.app.web.mappers.ObjectMapper;

public class LevelInfoHandler extends AbstractHandler {

    public static final String LEVEL_INFO_PREFIX = "levelinfo/";
    public static final String LEVEL_INFO_CONTEXT = "/" + LEVEL_INFO_PREFIX;
    private final Pattern pathUserId = Pattern.compile(LEVEL_INFO_PREFIX + "(\\d+)");
    
    private final UserService userService;
    private final ObjectMapper objectMapper;

    public LevelInfoHandler(ObjectMapper objectMapper, UserService userService) {
        this.objectMapper = objectMapper;
        this.userService = userService;
    }

    @Override
    protected void handleRequest(HttpExchange httpExchange) throws IOException {
        Integer levelId = getLevelId(httpExchange);
        Map<String, String> params = HttpHelper.getRequesParams(httpExchange);
        Integer topCount = Integer.valueOf(params.getOrDefault("topCount", "20"));
        List<UserInfoDto> list = userService.getTopUsersOnLevel(levelId, topCount)
                .stream()
                .map(user -> new UserInfoDto(user.getId(), levelId, user.getLevelTopResult(levelId)))
                .collect(Collectors.toList());
        HttpHelper.sendResultJson(httpExchange, HttpStatus.OK, objectMapper.toString(list));
    }

    private Integer getLevelId(HttpExchange httpExchange) throws IOException {
        Matcher matcher = pathUserId.matcher(httpExchange.getRequestURI().getPath());
        if(!matcher.find()) {
            throw new BadRequestException("can't parse levelId");
        }
        return Integer.parseInt(matcher.group(1));
    }

    @Override
    protected String getContextUrl() {
        return LEVEL_INFO_CONTEXT;
    }
}
