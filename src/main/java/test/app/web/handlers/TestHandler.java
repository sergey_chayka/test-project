package test.app.web.handlers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.sun.net.httpserver.HttpExchange;

import test.app.domain.UserInfo;
import test.app.services.UserService;

public class TestHandler extends AbstractHandler {

    private final UserService userService;
    
	public TestHandler(UserService userService) {
        this.userService = userService;
    }

    @Override
	public void handleRequest(HttpExchange httpExchange) throws IOException {
        Random r = new Random();
        for(int i = 1; i < 30; i++) {
            Map<Integer, Integer> map = new HashMap<>();
            UserInfo user = new UserInfo(i, map);
            for(int l = 0; l < 500; l++) {
                map.put(r.nextInt(50), r.nextInt(1000));
            }
            userService.saveUserInfo(user);
        }
	}

    @Override
    protected String getContextUrl() {
        return "/testdata";
    }

}
