package test.app.web.handlers;

import java.io.IOException;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import test.app.services.UserService;
import test.app.web.HttpHelper;
import test.app.web.HttpStatus;
import test.app.web.dto.UserInfoDto;
import test.app.web.mappers.ObjectMapper;

public class SetInfoHandler extends AbstractHandler {
    
    private final ObjectMapper objectMapper;
    private final UserService userService;

    public SetInfoHandler(ObjectMapper objectMapper, UserService userService) {
        this.objectMapper = objectMapper;
        this.userService = userService;
    }

    @Override
	public void handleRequest(HttpExchange httpExchange) throws IOException {
        String json = HttpHelper.readStream(httpExchange.getRequestBody());
        UserInfoDto userInfo = new Gson().fromJson(json, UserInfoDto.class);
        userService.addUserTopLevelResult(userInfo.userId, userInfo.levelId, userInfo.result);
        HttpHelper.sendResultJson(httpExchange, HttpStatus.OK,
                objectMapper.toString(userService.getUserInfo(userInfo.userId)));
	}
    
    @Override
    protected String getContextUrl() {
        return "/setinfo";
    }
}
