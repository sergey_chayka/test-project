package test.app.web.handlers;

public class BadRequestException extends RuntimeException {

    public BadRequestException(String string) {
        super(string);
    }

}
