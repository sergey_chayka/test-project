package test.app.web.handlers;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.sun.net.httpserver.HttpExchange;

import test.app.domain.UserInfo;
import test.app.services.UserService;
import test.app.web.HttpHelper;
import test.app.web.HttpStatus;
import test.app.web.dto.UserInfoDto;
import test.app.web.mappers.ObjectMapper;

public class UserInfoHandler extends AbstractHandler {
    
    public static final String USER_INFO_PREFIX = "userinfo/";
    public static final String USER_INFO_CONTEXT = "/" + USER_INFO_PREFIX;
    private final Pattern pathUserId = Pattern.compile(USER_INFO_PREFIX + "([0-9]+)");
    
    private final ObjectMapper objectMapper;
    private final UserService userService;

    public UserInfoHandler(ObjectMapper objectMapper, UserService userService) {
        this.objectMapper = objectMapper;
        this.userService = userService;
    }
    
    @Override
    protected String getContextUrl() {
        return USER_INFO_CONTEXT;
    }
    
    @Override
	public void handleRequest(HttpExchange httpExchange) throws IOException {
        Matcher matcher = pathUserId.matcher(httpExchange.getRequestURI().getPath());
        Integer userId = getUserId(httpExchange);
        if(matcher.find()) {
            userId = Integer.parseInt(matcher.group(1));
        }
        UserInfo userInfo = userService.getUserInfo(userId);
        List<UserInfoDto> result = userInfo.getLevels().stream()
            .map(levelId -> new UserInfoDto(userInfo.getId(), levelId, userInfo.getLevelTopResult(levelId)))
            .collect(Collectors.toList());
        HttpHelper.sendResultJson(httpExchange, HttpStatus.OK, objectMapper.toString(result));
	}

    private Integer getUserId(HttpExchange httpExchange) {
        Matcher matcher = pathUserId.matcher(httpExchange.getRequestURI().getPath());
        if(!matcher.find()) {
            throw new BadRequestException("can't parse userId");
        }
        return Integer.parseInt(matcher.group(1));
    }

}
