package test.app.web;

public enum HttpStatus {

    OK(200),
    BAD_REQUEST(400),
    INTERNAL_SERVER_ERROR(500)
    ;
    
    private final int code;

    private HttpStatus(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
