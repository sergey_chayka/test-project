package test.app.web.dto;

import com.google.gson.annotations.SerializedName;

public class UserInfoDto {
    
    public UserInfoDto() { }
    
    public UserInfoDto(Integer userId, Integer levelId, Integer result) {
        this.userId = userId;
        this.levelId = levelId;
        this.result = result;
    }

    @SerializedName("user_id")
    public Integer userId;
    
    @SerializedName("level_id")
    public Integer levelId;
    
    public Integer result;
}
