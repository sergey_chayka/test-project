package test.app.web.mappers;

import com.google.gson.Gson;

public class JsonObjectMapper implements ObjectMapper {

    private final Gson gson;
    
    public JsonObjectMapper() {
        this.gson = new Gson();
    }
    
    @Override
    public String toString(Object object) {
        return gson.toJson(object);
    }
    
    @Override
    public <T> T readObject(String str, Class<T> clazz) {
        return gson.fromJson(str, clazz);
    }
    
}
