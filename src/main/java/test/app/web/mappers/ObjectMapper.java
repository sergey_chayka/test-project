package test.app.web.mappers;

public interface ObjectMapper {

    String toString(Object object);

    <T> T readObject(String str, Class<T> clazz);
    
}
