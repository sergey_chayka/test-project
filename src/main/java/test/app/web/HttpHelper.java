package test.app.web;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;

public class HttpHelper {

    public static void sendResultJson(HttpExchange httpExchange, HttpStatus status, String response) throws IOException {
        httpExchange.getResponseHeaders().add("Content-Type", "application/json");
        byte[] bytes = response.getBytes();
        httpExchange.sendResponseHeaders(status.getCode(), bytes.length);
        try (OutputStream os = httpExchange.getResponseBody()) {
            os.write(bytes);
        }
    }

    public static String readStream(InputStream inputStream) throws IOException {
        int bufferSize = 4098;
        byte[] buffer = new byte[bufferSize];

        ByteArrayOutputStream os = new ByteArrayOutputStream(bufferSize);
        
        int readCnt;
        while ((readCnt = inputStream.read(buffer, 0, bufferSize)) != -1) {
            os.write(buffer, 0, readCnt);
        }
        return new String(os.toByteArray());
    }
    
    public static Map<String, String> getRequesParams(HttpExchange httpExchange) {
        Map<String, String> result = new HashMap<>();
        String query = httpExchange.getRequestURI().getQuery();
        if(query == null) {
            return result;
        }
        for (String part : query.split("&")) {
            String pair[] = part.split("=");
            if (pair.length > 1) {
                result.put(pair[0], pair[1]);
            } else {
                result.put(pair[0], "");
            }
        }
        return result;
    }

}
