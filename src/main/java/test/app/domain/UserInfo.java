package test.app.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Integer id;
    
    private final Map<Integer, Integer> topLevelsResult;
    
    public UserInfo(Integer id, Map<Integer, Integer> topLevelsResult) {
        this.id = id;
        this.topLevelsResult = topLevelsResult;
    }

    public Integer getId() {
        return id;
    }
    
    public HashMap<Integer, Integer> getTopLevelResults() {
        return new HashMap<>(topLevelsResult);
    }
    
    public Integer getLevelTopResult(Integer levelId) {
        return topLevelsResult.get(levelId);
    }

    public Set<Integer> getLevels() {
        return topLevelsResult.keySet();
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }
        if(obj instanceof UserInfo) {
            return id.equals(((UserInfo) obj).getId());
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
